# The script of the game goes in this file.
 
# Declare characters used by this game. The color argument colorizes the
# name of the character.
 
define h = Character("Hugh Mann", who_color="#4477f2")
define f = Character("Very Smart, Very Handsome Friend", who_color="#33f237")
#define d = Character("Douche Man") #unused?
define r = Character("Roommate", who_color="#f23337")
define b = Character("Barista")
define m = Character("Marqo's employee")
 
default water_run = False
default water_stop = False
 
default coffee_ground = False
default coffee_trash = False
default coffee_recycle = False
default coffee_rinse = False
 
default bottle_ground = False
default bottle_trash = False
default bottle_recycle = False
default bottle_rinse = False
 
default battery_ground = False
default battery_trash = False
default battery_recycle = False
default battery_btsu = False
 
default bag_ground = False
default bag_trash = False
default bag_recycle = False
default bag_btsu = False
 
default box_ground = False
default box_trash = False
default box_recycle = False
 
 
# The game starts here.
 
label start:
 
    scene bg dorm with fade
 
    show hugh with easeinleft
    
    "{i}Hugh Mann woke up at 8:30 A.M.{/i}"
    
    hide hugh with easeoutleft
    
    show roommate elation with easeinright
    
    r "Hey Hugh Mann, wuss poppin?"
    
    hide roommate with easeoutright
 
    show hugh with easeinleft
    
    h "Hey."
    
    hide hugh with easeoutleft
    
    scene bathroom with fade
    
    show hugh with easeinleft
    
    "{i}Hugh Mann and his roommate enter the restroom to brush their teeth. His roommate turns both sink handles 180 degrees and then promptly leaves the bathroom, muttering something about forgetting his toothbrush.{/i}"
    "{i} Hugh Mann, already holding a toothbrush with toothpaste on it, begins to brush his teeth.{/i}"
    
    hide hugh with easeoutleft
    
    menu:
        "{i}Does he leave the water running?{/i}"
        "Yes, he leaves the water running.":
            $ water_run = True
        "No, he does not leave the water running.":
            $ water_stop = True
            
    
    #TRANSITION HERE
    
    #coffee cup scene starts here
    scene bg dorm with fade

    show hugh with easeinleft
    
    h "{i}*Calls his very smart, very handsome friend*{/i} Hey do you want to go to the Bowen-Thompson Student Union with me and get some coffee?"
    
    hide hugh with easeoutleft
    
    show handsome with easeinright
    
    f "I’ll meet you there, at the Bowen-Thompson Student Union!"
    
    hide handsome with easeoutright
    
    scene btsu with fade
    
    "{i} Hugh Mann and his very smart, very handsome friend go to the Bowen-Thompson Student Union.{/i}"
    
    show hugh with easeinleft
    
    h "Let's go get coffee!!!"
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann turns to talk to the barista.{/i}"
 
    show hugh with easeinleft
    
    h "Can I get a regular grande coffee with milk and sugar? Thanks."
    
    hide hugh with easeoutleft

    show handsome with easeinright
    
    f "Can I get a matcha latte with soy milk and turmeric? Thanks, and here's my reusable cup!"
    
    hide handsome with easeoutright

    show worker with easeinright
    
    b "Alright! You save 10 cents!"
    
    hide worker with easeoutright
    
    "{i}Hugh Mann and his very smart, very handsome friend receive their drinks.{/i}"
 
    show hugh with easeinleft
    
    h "This coffee is rather unsatisfying."
    
    hide hugh with easeoutleft

    show handsome surprise with easeinright
    with vpunch
    
    f "Really? Mine's great!"
    
    hide handsome with easeoutright

    
    "{i}The two finish their drinks, and Hugh Mann's very smart, very handsome friend walks away to attend his ENVS 10500 class, leaving Hugh Mann with an empty coffee cup in his hand.{/i}"
 
    menu:
        "{i}What does he do with it?{/i}"
        "Throw it on the ground":
            $ coffee_ground = True
        "Throw it in the trash":
            $ coffee_trash = True
        "Throw it in the recycling":
            $ coffee_recycle = True
        "Rinse it out and reuse it":
            $ coffee_rinse = True
 

    show hugh with easeinleft
    with hpunch
 
    h "Wow look at the time, I'm late for my underwater basket weaving class!!"
    
    hide hugh with easeoutleft

    scene generic with fade
    
    "{i}Hugh Mann leaves the Bowen-Thompson Student Union and walks to his underwater basket weaving class{/i}"
    "{i}Time passes.{/i}"
 
    #plastic bottle scene starts
    "{i}Hugh Mann exits his underwater basket weaving class.{/i}"
    
    show hugh with easeinleft
    
    h "Wow I sure am thirsty after all that underwater basket weaving! I'll go buy my favorite sugary, carbonated beverage."
    
    hide hugh with easeoutleft

    scene vending with fade
    
    show hugh with easeinleft
    
    "{i}Hugh Mann walks over to the nearest vending machine and selects his drink.{/i}"
    
    "{i}CLUNK{/i}" with vpunch
    
    "{i}Hugh Mann opens the bottle and drinks until it's empty.{/i}"
    
    show hugh with easeinleft
    
    h "How refreshing! But what do I do with the bottle?"
    
    hide hugh with easeoutleft
 
    menu:
        "{i}What does he do with the bottle?{/i}"
        "Throw it on the ground":
            $ bottle_ground = True
        "Rinse it out and throw it in the recycling":
            $ bottle_rinse = True
        "Throw it in the trash":
            $ bottle_trash = True
        "Throw it in the recycling":
            $ bottle_recycle = True
        
        #"Reuse as water bottle"
            #set flag
            
    #electronic waste scene
    
    scene bg dorm with fade
    
    show hugh with easeinleft
    
    h "It seems like I have a free afternoon. I'm going to play Block Crafter World!"
    "{i}Hugh Mann pulls out his mouse and laptop and gets ready for a riveting game of Block Crafter World.{/i}"
    h "Holy Guacamole! My mouse is dead. :( I should change the batteries."
    "{i} Hugh Mann pulls the old batteries out of his mouse and replaces them with two new double A batteries.{/i}"
    h "Oh goodness, I have two old batteries now. What do I do?"
    
    hide hugh with easeoutleft
    
    menu:
        "{i}What should he do with the old batteries?{/i}"
        "Throw it in the recycling":
            $ battery_recycle = True
        "Place in recycling station at Jerome Library or in room 231 in the Bowen-Thompson Student Union.":
            $ battery_btsu = True
        "Throw it on the ground":
            $ battery_ground = True
        "Throw in the trash":
            $ battery_trash = True
            
    #plastic bag scene

    scene generic with fade

    "{i}Hugh Mann receives a call from his very smart, very handsome friend.{/i}"
    
    show handsome with easeinright
    
    f "Hey buddy ol' pal, do you want to go to the Carillon and get some dinner?"
    
    hide handsome with easeoutright
    
    show hugh with easeinleft
    
    h "Wow sure, getting dinner at the Carillon sounds like a great time!"
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann walks to the Carillon and meets his very smart, very handsome friend there to eat dinner.{/i}"
    "{i}After they leave, Hugh Mann feels that his meal was rather inadequate.{/i}"
    
    show hugh with easeinleft
    
    h "That was a rather inadequate meal."
    
    hide hugh with easeoutleft
    
    show handsome with easeinright
    
    f "Yeah, I got stuck with rice and beans again. :("
    
    hide handsome with easeoutright
    
    show hugh with easeinleft
    
    h "Do you want to go to Krogger? Let's get some adequate food!"
    
    hide hugh with easeoutleft
    
    show handsome with easeinright
    
    f "Heck yes!!!"
    
    hide handsome with easeoutright

    scene shelves with fade
    
    "{i}The two friends go to Krogger to get some adequate food. While in Krogger, they pick up some groceries and then head over to the cashier to check out.{/i}"
    
    show handsome with easeinright
    
    f "Luckily, I brought my own reusable bags!"
    
    hide handsome with easeoutright
    
    show hugh with easeinleft
    
    h "I didn't. :("
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann receives plastic bags for his groceries and adequate food.{/i}"
    
    show handsome with easeinright
    
    f "Hey, I'm going to head back to my dorm to unpack my groceries! Do you want to meet again tomorrow at the Bowen-Thompson Student Union before your underwater basket weaving class?"
    
    hide handsome with easeoutright
    
    show hugh with easeinleft
    
    h "Sounds like a plan! See you tomorrow at the Bowen-Thompson Student Union!!"
    
    hide hugh with easeoutleft
    
    scene bg dorm with fade
    
    show hugh with easeinleft
    
    "{i}Hugh Mann walks back to his dorm and unpacks his groceries, leaving him with a number of empty, plastic grocery bags.{/i}"
    h "Hmmm, I wonder what to do with all these plastic bags."
    
    menu:
        "{i}What does he do with all the plastic bags?{/i}"
        "Throw them in the plastic bag recycling at the Bowen-Thompson Student Union or Falcon Height":
            $ bag_btsu = True
        "Throw them on the ground":
            $ bag_ground = True
        "Throw them in the recycling":
            $ bag_recycle = True
        "Throw them in the trash":
            $ bag_trash = True
        
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann relaxes in his room until his roommate bursts in, obviously hungry.{/i}"
 
    #pizza box scene
    
    show roommate happy with easeinright
    
    r "Hey nerd do you wanna come grab Marqo’s pizza with me at the Bowen-Thompson Student Union?"
    
    hide roommate with easeoutright
    
    show hugh with easeinleft
    
    h "Sure! I'm pretty hungry and would enjoy some Marqo's pizza at the Bowen-Thompson Student Union!"
    
    hide hugh with easeoutleft

    scene btsu with fade
    
    "{i}Hugh Mann and his roommate walk over to the Bowen-Thompson Student Union and stand in line at Marqo’s.{/i}"
    
    show worker with easeinright
    
    m "Welcome to Marqo’s Pizza! How can I help you?"
    
    hide worker with easeoutright
    
    show roommate elation with easeinright
    
    r "Can I get an extra-large all-meat pizza with, uhh, a steak and cheese sub?"
    
    hide roommate with easeoutright
    
    show hugh with easeinleft
    
    h "And can I get a medium cheese pizza?"
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann and his roomate grab their pizza and sit down to eat.{/i}"
    
    show hugh with easeinleft
    
    h "This pizza is pretty boring and unexciting. :("
    
    hide hugh with easeoutleft
    
    show roommate anger with easeinright
    
    r "Actually, I don’t think I’m that hungry, I only ate one slice. I’m gonna go toss this pizza box then head out."
    
    hide roommate with easeoutright
    
    show hugh with easeinleft
    
    h "Oh, alright. Have a nice night!"
    
    hide hugh with easeoutleft
    
    "{i}Hugh Mann’s roommate tosses the entire pizza box, along with the pizza, into the recycling bin. As he leaves the Bowen-Thompson Student Union, he can be seen throwing his sub wrapper at some squirrels.{/i}"
    
    "{i}They attack him.{/i}" with vpunch
    
    show hugh with easeinleft
    
    h "Mmm, I’ll have to throw away my pizza box too."
    
    hide hugh with easeoutleft
    
    menu:
        "{i}What should he do?{/i}"
        "Throw it in the trash":
            $ box_trash = True
        "Throw it on the ground":
            $ box_ground = True
        "Throw it in the recycling":
            $ box_recycle = True

label ending:

    "{i}Hugh Mann goes to sleep at 11:45 P.M.{/i}"


    #ending screen starts here
    
    #change bg to bathroom

    scene bathroom with fade

    "Recalling what happened that morning in the bathroom..."

    if water_stop:
        "{i}Congratulations! You saved water and money by not letting the water run while Hugh Mann brushed his teeth!{/i}"
    elif water_run:
        "{i}Oh no! You wasted fresh water by letting the water run while Hugh Mann brushed his teeth!{/i}"

    "Fresh water is a precious resource! By reducing use, we avoid needing to recycle the grey water in a water treatment plant."

    #change bg to btsu
    scene btsu with fade

    "Recalling what happened after getting some coffee..."

    if coffee_ground:
        "{i}Cup on gorund is bad.{/i}"
    elif coffee_trash:
        "{i}Not terrible, but could be better.{/i}"
    elif coffee_recycle:
        "{i}Don't put it in the recycling without rinsing it!{/i}"
    elif coffee_rinse:
        "{i}Yes! Reusing coffee cups is the best way to deal with them.{/i}"
        
    #change bg to vending machine
    scene vending with fade

    "Recalling what happened after getting a refreshing carbonated sugary beverage..."
    
    if bottle_ground:
        "{i}Be careful, you might trip on your trash.{/i}"
    elif bottle_rinse:
        "{i}Congratulations! This is the right thing to do.{/i}"
    elif bottle_trash:
        "{i}Unfortunately, plastic bottles last a very long time in landfills.{/i}"
    elif bottle_recycle:
        "{i}A small amount of liquid remaining in a bottle can cause contamination, harming recyling machines.{/i}"
    
    "{i}Rinsing bottles before recycling is the ideal method to ensure that no contamination accidently ocours.{/i}"

    #change bg to dorm?
    scene bg dorm with fade

    "Recalling the battery..."
    
    if battery_recycle:
        "{i}Your intentions were good, but placing the batteries in the normal recycling will likely contaminate the load, causing EVERYTHING else to be sent to the landfill!{/i}"
    elif battery_btsu:
        "{i}Good job! Proper recycling containers can be found at these locations and many others that can be found on the BGSU website!{/i}"
    elif battery_ground:
        "{i}This is not a good idea! This is dangerous for squirrels, birds, and other small critters{/i}"
    elif battery_trash:
        "{i}Batteries are actually able to be recycled! This way we can reduce waste and keep dangerous chemicals from leaking into the soil and groundwater{/i}"

    #change bg to btsu
    scene btsu with fade

    "Recalling the plastic bag..."
    
    if bag_btsu:
        "{i}Great choice! The dedicated bag recycling receptical at the union is an idea place to recycling bags.{/i}"
    elif bag_ground:
        "{i}Are you trying to hurt little animals? That is a very bad thing to do.{/i}"
    elif bag_recycle:
        "{i}Unfrotunately, plastic bags can jam and damage regular recycling machines, making it harder to offer recycling services.{/i}"
    elif bag_trash:
        "{i}This is a poor option, only slightly better then placing plastic bags in regular recycling{/i}"
    "Plastic bags can be recycled, however they require specilized equipment. Plastic bags in regular recycling can damage recycling machines."
    "There is a special plastic bag recycling bin in the Bowen Thompson Student Union, and many supermarkets also take bags."

    #change bg to btsu

    "Recalling the pizza box..."
    
    if box_trash:
        "{i}Good choice! Given the circumstances of campus life, this is the best option.{/i}"
    elif box_ground:
        "{i}While composting pizza boxes is possible, you need to do a better job then throwing it on the ground.{/i}"
    elif box_recycle:
        "{i}If only placing the pizza box in the recycling was a good idea. Alas, it is not.{/i}"
    "{i}Pizza Boxes are often contaminated by greese, and other contaminants that can ruin a whole load of recycling, turning it into trash{/i}"

    "Thanks for playing! Visit the {a=https://www.bgsu.edu/campus-sustainability/recycling-waste-reduction.html}BGSU sustainability website{/a} for more information"

    # This ends the game.
 
    return
